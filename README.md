# jlox: a Lox interpreter in Java

Implementing the Lox language described in the book [*Crafting Interpreters*](http://craftinginterpreters.com/), and adding some of my own ideas along the way.

Automatically generated classes like `Expr` and `Stmt` are not included in this repo. Run `make regen` to generate them.

## Added features not in the book

- **Escape sequences in strings:** The scanner recognizes and interprets the follows escape sequences in string literals:
  - `\"`: literal double quote
  - `\n`: newline
  - `\r`: carriage return
  - `\t`: tab
  - `\xXX`: arbitrary byte in hexadecimal (`XX` is exactly two hexadecimal digits)
  - `\` immediately followed by a newline: literal newline
  - `\\`: literal backslash
  - Any other sequence beginning with `\` is invalid.
- **Single-line strings:** String literals now cannot span across multiple lines (which is usually a mistake), unless the backslash escape above is used.
- **Improved number literals:**
  - Integer number literals can be specified in hexadecimal by using a `0x` prefix. Both upper and lower case are supported for letter digits as well as the prefix.
  - Scientific notation is supported (e.g `1.234e5`).
- **Additional operators:**
  - `^` for exponentiation
  - `%` for remainder operation (integers only, result has same sign as right operand)
- **Division by zero** now throws an exception.
- **Library functions:** An easy way to define native functions, and a small library of helpful functions.
- **Print function:** `print` has been converted to a library function instead of a statement.
- **Break and continue statements** are now supported in loops.
- **Parentheses-less if and while conditions:** `if` and `while` conditions no longer require surrounding parentheses; instead they must be followed by a braced block.
