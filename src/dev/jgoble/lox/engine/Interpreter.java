package dev.jgoble.lox.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dev.jgoble.lox.Lox;
import dev.jgoble.lox.ast.Expr;
import dev.jgoble.lox.ast.Stmt;
import dev.jgoble.lox.exceptions.Return;
import dev.jgoble.lox.exceptions.RuntimeError;
import dev.jgoble.lox.internal.Environment;
import dev.jgoble.lox.internal.Token;
import dev.jgoble.lox.internal.TokenType;
import dev.jgoble.lox.lib.Base;
import dev.jgoble.lox.types.Function;
import dev.jgoble.lox.types.LoxCallable;
import dev.jgoble.lox.types.LoxClass;
import dev.jgoble.lox.types.LoxFloat;
import dev.jgoble.lox.types.LoxFunction;
import dev.jgoble.lox.types.LoxInstance;
import dev.jgoble.lox.types.LoxString;

public class Interpreter implements Expr.Visitor<Object>, Stmt.Visitor<Void> {
    public final Environment globals = new Environment();
    private Environment environment = globals;
    private final Map<Expr, Integer> locals = new HashMap<>();

    public void init() {
        Base.loadLibrary(environment);
    }

    public void interpret(List<Stmt> statements) {
        try {
            for (Stmt statement : statements) {
                execute(statement);
            }
        } catch (RuntimeError e) {
            Lox.runtimeError(e);
        }
    }

    public String stringify(Object object) {
        if (object == null) {
            return "nil";
        }
        return object.toString();
    }

    @Override
    public Void visitClassStmt(Stmt.Class stmt) {
        Object superclass = null;
        if (stmt.superclass != null) {
            superclass = evaluate(stmt.superclass);
            if (!(superclass instanceof LoxClass)) {
                throw new RuntimeError(stmt.superclass.name, "Superclass is not a class");
            }
        }
        environment.define(stmt.name.lexeme, null);
        if (superclass != null) {
            environment = new Environment(environment);
            environment.define("super", superclass);
        }
        Map<String, Function> methods = new HashMap<>();
        for (Stmt.Function method : stmt.methods) {
            LoxFunction function = new LoxFunction(method, environment, method.name.lexeme.equals("init"));
            methods.put(method.name.lexeme, function);
        }
        LoxClass klass = new LoxClass(stmt.name.lexeme, (LoxClass)superclass, methods);
        if (superclass != null) {
            environment = environment.enclosing;
        }
        environment.assign(stmt.name, klass);
        return null;
    }

    @Override
    public Void visitBlockStmt(Stmt.Block stmt) {
        executeBlock(stmt.statements, new Environment(environment));
        return null;
    }

    @Override
    public Void visitIfStmt(Stmt.If stmt) {
        if (isTruthy(evaluate(stmt.condition))) {
            execute(stmt.thenBranch);
        } else if (stmt.elseBranch != null) {
            execute(stmt.elseBranch);
        }
        return null;
    }

    @Override
    public Void visitExpressionStmt(Stmt.Expression stmt) {
        evaluate(stmt.expression);
        return null;
    }

    @Override
    public Void visitVarStmt(Stmt.Var stmt) {
        Object value = stmt.initializer != null ? evaluate(stmt.initializer) : null;
        environment.define(stmt.name.lexeme, value);
        return null;
    }

    @Override
    public Void visitWhileStmt(Stmt.While stmt) {
        while (isTruthy(evaluate(stmt.condition))) {
            try {
                execute(stmt.body);
            } catch (Break e) {
                break;
            } catch (Continue e) {
                continue;
            }
        }
        return null;
    }

    @SuppressWarnings("serial")
    private class Break extends RuntimeException {
        Break() {
            super(null, null, false, false);
        }
    }

    @Override
    public Void visitBreakStmt(Stmt.Break stmt) {
        throw new Break();
    }

    @SuppressWarnings("serial")
    private class Continue extends RuntimeException {
        Continue() {
            super(null, null, false, false);
        }
    }

    @Override
    public Void visitContinueStmt(Stmt.Continue stmt) {
        throw new Continue();
    }

    @Override
    public Void visitFunctionStmt(Stmt.Function stmt) {
        LoxFunction function = new LoxFunction(stmt, environment, false);
        environment.define(stmt.name.lexeme, function);
        return null;
    }

    @Override
    public Void visitReturnStmt(Stmt.Return stmt) {
        Object value = stmt.value != null ? evaluate(stmt.value) : null;
        throw new Return(value);
    }

    @Override
    public Object visitAssignExpr(Expr.Assign expr) {
        Object value = evaluate(expr.value);
        Integer distance = locals.get(expr);
        if (distance != null) {
            environment.assignAt(distance, expr.name, value);
        } else {
            globals.assign(expr.name, value);
        }
        return value;
    }

    @Override
    public Object visitLiteralExpr(Expr.Literal expr) {
        return expr.value;
    }

    @Override
    public Object visitGroupingExpr(Expr.Grouping expr) {
        return evaluate(expr.expression);
    }

    @Override
    public Object visitUnaryExpr(Expr.Unary expr) {
        Object right = evaluate(expr.right);
        switch (expr.operator.type) {
            case TOKEN_BANG:
                return !isTruthy(right);
            case TOKEN_MINUS:
                checkNumberOperand(expr.operator, right);
                return ((LoxFloat)right).__negate();
            default:
                // Unreachable
                return null;
        }
    }

    @Override
    public Object visitVariableExpr(Expr.Variable expr) {
        return lookUpVariable(expr.name, expr);
    }

    private Object lookUpVariable(Token name, Expr expr) {
        Integer distance = locals.get(expr);
        if (distance != null) {
            return environment.getAt(distance, name.lexeme);
        } else {
            return globals.get(name);
        }
    }

    @Override
    public Object visitBinaryExpr(Expr.Binary expr) {
        Object left = evaluate(expr.left);
        Object right = evaluate(expr.right);
        switch (expr.operator.type) {
            case TOKEN_MINUS:
                checkNumberOperands(expr.operator, left, right);
                return ((LoxFloat)left).__subtract((LoxFloat)right);
            case TOKEN_SLASH:
                checkNumberOperands(expr.operator, left, right);
                if (right.equals(new LoxFloat(0.0))) {
                    throw new RuntimeError(expr.operator, "Division by zero");
                }
                return ((LoxFloat)left).__divide((LoxFloat)right);
            case TOKEN_STAR:
                checkNumberOperands(expr.operator, left, right);
                return ((LoxFloat)left).__multiply((LoxFloat)right);
            case TOKEN_PERCENT: {
                checkIntegerOperands(expr.operator, left, right);
                if (right.equals(new LoxFloat(0.0))) {
                    throw new RuntimeError(expr.operator, "Division by zero");
                }
                return ((LoxFloat)left).__mod((LoxFloat)right);
            }
            case TOKEN_CARET:
                checkNumberOperands(expr.operator, left, right);
                return ((LoxFloat)left).__pow((LoxFloat)right);
            case TOKEN_PLUS:
                if (left instanceof LoxFloat && right instanceof LoxFloat) {
                    return ((LoxFloat)left).__add((LoxFloat)right);
                }
                if (left instanceof LoxString && right instanceof LoxString) {
                    return ((LoxString)left).__add((LoxString)right);
                }
                throw new RuntimeError(expr.operator, "Operands must be two numbers or two strings");
            case TOKEN_GREATER:
                checkNumberOperands(expr.operator, left, right);
                return ((LoxFloat)left).__gt((LoxFloat)right);
            case TOKEN_GREATER_EQUAL:
                checkNumberOperands(expr.operator, left, right);
                return ((LoxFloat)left).__ge((LoxFloat)right);
            case TOKEN_LESS:
                checkNumberOperands(expr.operator, left, right);
                return ((LoxFloat)left).__lt((LoxFloat)right);
            case TOKEN_LESS_EQUAL:
                checkNumberOperands(expr.operator, left, right);
                return ((LoxFloat)left).__le((LoxFloat)right);
            case TOKEN_BANG_EQUAL:
                return !isEqual(left, right);
            case TOKEN_EQUAL_EQUAL:
                return isEqual(left, right);
            default:
                // Unreachable
                return null;
        }
    }

    @Override
    public Object visitCallExpr(Expr.Call expr) {
        Object callee = evaluate(expr.callee);
        List<Object> arguments = new ArrayList<>();
        for (Expr argument : expr.arguments) {
            arguments.add(evaluate(argument));
        }
        if (!(callee instanceof LoxCallable)) {
            throw new RuntimeError(expr.paren, "Attempt to call a non-callable value");
        }
        LoxCallable function = (LoxCallable)callee;
        if (arguments.size() != function.arity()) {
            throw new RuntimeError(expr.paren, "Expected " + function.arity() + " arguments, got " + arguments.size());
        }
        return function.call(this, arguments);
    }

    @Override
    public Object visitGetExpr(Expr.Get expr) {
        Object object = evaluate(expr.object);
        if (object instanceof LoxInstance) {
            return ((LoxInstance)object).get(expr.name);
        }
        throw new RuntimeError(expr.name, "Property access on non-instance");
    }

    @Override
    public Object visitSetExpr(Expr.Set expr) {
        Object object = evaluate(expr.object);
        if (!(object instanceof LoxInstance)) {
            throw new RuntimeError(expr.name, "Cannot set field on non-instance");
        }
        Object value = evaluate(expr.value);
        ((LoxInstance)object).set(expr.name, value);
        return value;
    }

    @Override
    public Object visitSuperExpr(Expr.Super expr) {
        int distance = locals.get(expr);
        LoxClass superclass = (LoxClass)environment.getAt(distance, "super");
        LoxInstance object = (LoxInstance)environment.getAt(distance - 1, "this");
        Function method = superclass.findMethod(expr.method.lexeme);
        if (method == null) {
            throw new RuntimeError(expr.method, "Undefined property '" + expr.method.lexeme + "'");
        }
        return method.bind(object);
    }

    @Override
    public Object visitThisExpr(Expr.This expr) {
        return lookUpVariable(expr.keyword, expr);
    }

    @Override
    public Object visitLogicalExpr(Expr.Logical expr) {
        Object left = evaluate(expr.left);
        if (expr.operator.type == TokenType.TOKEN_OR) {
            if (isTruthy(left)) {
                return left;
            }
        } else {
            if (!isTruthy(left)) {
                return left;
            }
        }
        return evaluate(expr.right);
    }

    private boolean isTruthy(Object object) {
        if (object == null) {
            return false;
        }
        if (object instanceof Boolean) {
            return (boolean)object;
        }
        return true;
    }

    private boolean isEqual(Object a, Object b) {
        if (a == null) {
            return (b == null);
        }
        return a.equals(b);
    }

    private Object evaluate(Expr expr) {
        return expr.accept(this);
    }

    private void execute(Stmt stmt) {
        stmt.accept(this);
    }

    public void executeBlock(List<Stmt> statements, Environment environment) {
        Environment previous = this.environment;
        try {
            this.environment = environment;
            for (Stmt statement : statements) {
                execute(statement);
            }
        } finally {
            this.environment = previous;
        }
    }

    private void checkNumberOperand(Token operator, Object operand) {
        if (!(operand instanceof LoxFloat)) {
            throw new RuntimeError(operator, "Operand must be a number");
        }
    }

    private void checkNumberOperands(Token operator, Object left, Object right) {
        if (!(left instanceof LoxFloat && right instanceof LoxFloat)) {
            throw new RuntimeError(operator, "Operands must be numbers");
        }
    }

    private void checkIntegerOperands(Token operator, Object left, Object right) {
        if (!(left instanceof LoxFloat && right instanceof LoxFloat)) {
            throw new RuntimeError(operator, "Operands must be integers");
        }
        double leftDouble = ((LoxFloat)left).value;
        double rightDouble = ((LoxFloat)right).value;
        if (leftDouble != (long)leftDouble || rightDouble != (long)rightDouble) {
            throw new RuntimeError(operator, "Operands must be integers");
        }
    }

    void resolve(Expr expr, int depth) {
        locals.put(expr, depth);
    }
}
