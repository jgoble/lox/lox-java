package dev.jgoble.lox.types;

import java.util.List;
import java.util.Map;

import dev.jgoble.lox.engine.Interpreter;

public class LoxClass implements LoxCallable {
    final String name;
    final LoxClass superclass;
    protected final Map<String, Function> methods;

    public LoxClass(String name, LoxClass superclass, Map<String, Function> methods) {
        this.name = name;
        this.superclass = superclass;
        this.methods = methods;
    }

    public Function findMethod(String name) {
        if (methods.containsKey(name)) {
            return methods.get(name);
        }
        if (superclass != null) {
            return superclass.findMethod(name);
        }
        return null;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int arity() {
        Function initializer = findMethod("init");
        return initializer == null ? 0 : initializer.arity();
    }

    @Override
    public Object call(Interpreter interpreter, List<Object> arguments) {
        LoxInstance instance = new LoxInstance(this);
        Function initializer = findMethod("init");
        if (initializer != null) {
            initializer.bind(instance).call(interpreter, arguments);
        }
        return instance;
    }

    @Override
    public String toString() {
        return name;
    }
}
