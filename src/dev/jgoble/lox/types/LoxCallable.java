package dev.jgoble.lox.types;

import java.util.List;

import dev.jgoble.lox.engine.Interpreter;

public interface LoxCallable {
    String getName();
    int arity();
    Object call(Interpreter interpreter, List<Object> arguments);
}
