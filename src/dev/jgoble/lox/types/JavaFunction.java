package dev.jgoble.lox.types;

import java.util.List;

import dev.jgoble.lox.internal.Environment;
import dev.jgoble.lox.Lox;
import dev.jgoble.lox.engine.Interpreter;

public class JavaFunction implements Function {
    final String name;
    private final int nargs;
    private final NativeCode code;
    private final Environment closure;

    @FunctionalInterface
    public interface NativeCode {
        public Object call(Interpreter interp, List<Object> args, Environment env);
    }

    public JavaFunction(String name, int nargs, NativeCode code) {
        this.name = name;
        this.nargs = nargs;
        this.code = code;
        this.closure = Lox.interpreter.globals;
    }

    JavaFunction(String name, int nargs, NativeCode code, Environment closure) {
        this.name = name;
        this.nargs = nargs;
        this.code = code;
        this.closure = closure;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int arity() {
        return nargs;
    }

    @Override
    public Object call(Interpreter interpreter, List<Object> arguments) {
        return code.call(interpreter, arguments, closure);
    }

    @Override
    public Function bind(LoxInstance instance) {
        Environment environment = new Environment(closure);
        environment.define("this", instance);
        return new JavaFunction(name, nargs, code, environment);
    }

    @Override
    public String toString() {
        return "<native fn " + name + ">";
    }
}
