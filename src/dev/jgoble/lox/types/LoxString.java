package dev.jgoble.lox.types;

public class LoxString extends LoxInstance {
    public final String value;

    public LoxString(String value) {
        super(LoxStringClass.getInstance());
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public LoxString __add(LoxString right) {
        return new LoxString(value + right.value);
    }
}
